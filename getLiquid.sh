#!/usr/bin/bash
while getopts f: o
do	case "$o" in
	f)	force="$OPTARG";;
	\?)	print >&2 "Usage: $0 f=y"
		exit 1;;
	esac
done
shift $((OPTIND -1))

echo "force = $force"

if [ "$force" != "y" ]; then 
    echo "Warning - This is a destructive reset of home environmnent"
    echo "Waiting 20 seconds for you to kill"
    sleep 20
fi

mkdir ~/apps
pushd .
cd ~/apps
rm -rf liquidprompt
git clone https://github.com/nojhan/liquidprompt.git
popd
echo "source ~/apps/liquidprompt/liquidprompt" >> ~/.bashrc
echo "Done modifying .bashrc"

